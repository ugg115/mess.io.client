﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum ConnectionStatus {
    Connected, Disconnected, Reconnecting
}

class ConnectionUpdate {
    public ConnectionStatus Status { get; }
    public string Message { get; }

    public ConnectionUpdate(ConnectionStatus status, string message) {
        Status = status;
        Message = message;
    }
}

