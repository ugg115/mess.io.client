﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ConsoleWriter {

    IList<Config> _textBuffer;

    public ConsoleWriter() {
        _textBuffer = new List<Config>();
    }
    
    public ConsoleWriter Append(object obj, ConsoleColor? foreground = null, ConsoleColor? background = null) {
        _textBuffer.Add(new Config {
            Writable = obj,
            Foreground = foreground,
            Background = background,
            WriteAction = Console.Write
        });

        return this;
    }
    
    public ConsoleWriter AppendLine(object obj, ConsoleColor? foreground = null, ConsoleColor? background = null) {
        _textBuffer.Add(new Config {
            Writable = obj,
            Foreground = foreground,
            Background = background,
            WriteAction = Console.WriteLine
        });

        return this;
    }

    public void Print() {
        foreach (var config in _textBuffer) {
            if (config.Foreground.HasValue) {
                Console.ForegroundColor = config.Foreground.Value;
            }

            if (config.Background.HasValue) {
                Console.BackgroundColor = config.Background.Value;
            }

            config.WriteAction(config.Writable);

            Console.ResetColor();
        }
    }

    class Config {
        public ConsoleColor? Foreground { get; set; }
        public ConsoleColor? Background { get; set; }
        public object Writable { get; set; }
        public Action<object> WriteAction { get; set; }
    }
}
