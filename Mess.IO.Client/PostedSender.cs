﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class PostedSender {
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("action")]
    public string Action { get; set; }
}
