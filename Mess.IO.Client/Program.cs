﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using static System.Console;

class Program : IObserver<PostedMessage>, IObserver<PostedSender>, IObserver<ConnectionUpdate> {

    static TransformBlock<string[], string> _nameEntry = new TransformBlock<string[], string>(args => {
        while(true) {
            Write("Enter your display name...\r\n>>");
            var name = ReadLine();

            if (!string.IsNullOrWhiteSpace(name))
                return name;
        }
    });

    static ActionBlock<string> _mainClientLoop = new ActionBlock<string>(async sender => {
        var client = new Client();
        var program = new Program();
        client.MessagePosted.Subscribe(program);
        client.SenderPosted.Subscribe(program);

        await client.Connect(sender);
        WriteLine("Connected");

        while (true) {
            var text = ReadLine();

            if (!string.IsNullOrWhiteSpace(text)) {
                await client.Post(text);
            }
        }
    });

    static void Main(string[] args) {
        var linkOptions = new DataflowLinkOptions {
            PropagateCompletion = true
        };

        _nameEntry.LinkTo(_mainClientLoop);
        _nameEntry.Post(args);
        _nameEntry.Complete();

        try {
            _mainClientLoop.Completion.Wait();
        } catch(Exception ex) {
            new ConsoleWriter()
                .AppendLine("=========== ERROR =============", ConsoleColor.White, ConsoleColor.DarkGray)
                .AppendLine(ex)
                .Print(); 
        }
    }

    void IObserver<PostedMessage>.OnCompleted() {
        
    }

    void IObserver<PostedSender>.OnCompleted() {
        
    }

    void IObserver<ConnectionUpdate>.OnCompleted() {
        
    }

    void IObserver<PostedMessage>.OnError(Exception error) {
        
    }

    void IObserver<PostedSender>.OnError(Exception error) {
        
    }

    void IObserver<ConnectionUpdate>.OnError(Exception error) {
        
    }

    void IObserver<PostedMessage>.OnNext(PostedMessage value) {
        new ConsoleWriter()
            .Append($"[{value.TimeStamp.LocalDateTime.ToString("M/d h:mm:ss tt")}]", ConsoleColor.DarkGray)
            .Append(" - ")
            .Append(value.Sender, ConsoleColor.Cyan)
            .Append(" > ")
            .AppendLine(value.Text, ConsoleColor.Yellow)
            .Append(">> ")
            .Print();
    }

    void IObserver<PostedSender>.OnNext(PostedSender value) {
        new ConsoleWriter()
            .AppendLine($"{value.Name} {value.Action}")
            .Append(">> ")
            .Print(); ;
    }

    void IObserver<ConnectionUpdate>.OnNext(ConnectionUpdate value) {
        new ConsoleWriter()
            .AppendLine($"Connection Change - {value.Status} > {value.Message}", ConsoleColor.White, ConsoleColor.Gray)
            .Append(">> ")
            .Print();
    }
}
