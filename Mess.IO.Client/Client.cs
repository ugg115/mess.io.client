﻿using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

class Client {
    const string MessagePostedKey = "message:posted";
    const string SenderPostedJoined = "sender:posted:joined";

    Socket _socket;
    HttpClient _http;
    string _sender;
    BufferBlock<PostedMessage> _messageBuffer;
    BufferBlock<PostedSender> _senderBuffer;
    BufferBlock<ConnectionUpdate> _connectionAction;

    public IObservable<PostedMessage> MessagePosted { get; }
    public IObservable<PostedSender> SenderPosted { get; }
    public IObservable<ConnectionUpdate> ConnectionChanged { get; }

    public Client() {
        _socket = IO.Socket("https://messio.azurewebsites.net");
        _http = new HttpClient();
        _messageBuffer = new BufferBlock<PostedMessage>();
        _senderBuffer = new BufferBlock<PostedSender>();
        _connectionAction = new BufferBlock<ConnectionUpdate>();

        MessagePosted = _messageBuffer.AsObservable();
        SenderPosted = _senderBuffer.AsObservable();
        ConnectionChanged = _connectionAction.AsObservable();

        _socket.On(MessagePostedKey, data => {
            var message = Deserialize<PostedMessage>(data);
            _messageBuffer.Post(message);
        });

        _socket.On(SenderPostedJoined, data => {
            var sender = Deserialize<PostedSender>(data);
            _senderBuffer.Post(sender);
        });

        _socket.On(Socket.EVENT_CONNECT, data => {
            PostConnectionChange(ConnectionStatus.Connected, "Connected");
        });

        _socket.On(Socket.EVENT_CONNECT_ERROR, data => {
            PostConnectionChange(ConnectionStatus.Disconnected, "Connection Failed");
        });

        _socket.On(Socket.EVENT_DISCONNECT, data => {
            PostConnectionChange(ConnectionStatus.Disconnected, "Disconnecdted");
        });

        _socket.On(Socket.EVENT_RECONNECTING, data => {
            PostConnectionChange(ConnectionStatus.Reconnecting, "Reconnecting...");
        });
    }

    public Task Connect(string sender) {
        _sender = sender;
        _socket.Connect();
        var json = JsonConvert.SerializeObject(new { name = sender });
        var content = new StringContent(json, Encoding.UTF8, "application/json");
        return _http.PostAsync("http://messio.azurewebsites.net/api/v1/senders/joined", content);
    }

    public async Task<PostedMessage> Post(string text) {
        var data = JsonConvert.SerializeObject(new {
            text,
            sender = _sender
        });

        var content = new StringContent(data, Encoding.UTF8, "application/json");
        var response = await _http.PostAsync("http://messio.azurewebsites.net/api/v1/messages", content);

        var messageResponse = await response.Content.ReadAsStringAsync();
        try {
            var post = JsonConvert.DeserializeObject<PostedMessage>(messageResponse);
            return post;
        } catch(Exception ex) {
            return null;
        }
        
    }

    T Deserialize<T>(object data) {
        return JsonConvert.DeserializeObject<T>(data.ToString());
    }

    void PostConnectionChange(ConnectionStatus status, string message) {
        _connectionAction.Post(new ConnectionUpdate(status, message));
    }
}
    
