﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class PostedMessage {
    [JsonProperty("sender")]
    public string Sender { get; set; }

    [JsonProperty("text")]
    public string Text { get; set; }

    [JsonProperty("timeStamp")]
    public DateTimeOffset TimeStamp { get; set; }
}
